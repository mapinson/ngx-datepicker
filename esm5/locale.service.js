import { __assign, __decorate, __param } from "tslib";
import { Injectable, Inject } from '@angular/core';
import { LOCALE_CONFIG, DefaultLocaleConfig } from './daterangepicker.config';
import * as ɵngcc0 from '@angular/core';
var LocaleService = /** @class */ (function () {
    function LocaleService(_config) {
        this._config = _config;
    }
    Object.defineProperty(LocaleService.prototype, "config", {
        get: function () {
            if (!this._config) {
                return DefaultLocaleConfig;
            }
            return __assign(__assign({}, DefaultLocaleConfig), this._config);
        },
        enumerable: true,
        configurable: true
    });
    LocaleService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
    ]; };
    LocaleService = __decorate([ __param(0, Inject(LOCALE_CONFIG))
    ], LocaleService);
LocaleService.ɵfac = function LocaleService_Factory(t) { return new (t || LocaleService)(ɵngcc0.ɵɵinject(LOCALE_CONFIG)); };
LocaleService.ɵprov = ɵngcc0.ɵɵdefineInjectable({ token: LocaleService, factory: function (t) { return LocaleService.ɵfac(t); } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵngcc0.ɵsetClassMetadata(LocaleService, [{
        type: Injectable
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [LOCALE_CONFIG]
            }] }]; }, null); })();
    return LocaleService;
}());
export { LocaleService };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlLnNlcnZpY2UuanMiLCJzb3VyY2VzIjpbIm5neC1kYXRlcmFuZ2VwaWNrZXItbWF0ZXJpYWwvbG9jYWxlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxhQUFhLEVBQUUsbUJBQW1CLEVBQWdCLE1BQU0sMEJBQTBCLENBQUM7O0FBRzVGO0lBQ0UsdUJBQTJDLE9BQXFCO1FBQXJCLFlBQU8sR0FBUCxPQUFPLENBQWM7SUFBRyxDQUFDO0lBRXBFLHNCQUFJLGlDQUFNO2FBQVY7WUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDakIsT0FBTyxtQkFBbUIsQ0FBQzthQUM1QjtZQUVELDZCQUFZLG1CQUFtQixHQUFLLElBQUksQ0FBQyxPQUFPLEVBQUU7UUFDcEQsQ0FBQzs7O09BQUE7O2dEQVJZLE1BQU0sU0FBQyxhQUFhOztJQUR0QixhQUFhLGVBQ2QsU0FGWCxVQUFVLEVBQUUsckJBQ0wsQ0FDTyxXQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtPQUR2QixhQUFhLENBVXpCOzs7Ozs7OztrQ0FDRDtJQURBLG9CQUFDO0NBQUEsQUFWRCxJQVVDO1NBVlksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTE9DQUxFX0NPTkZJRywgRGVmYXVsdExvY2FsZUNvbmZpZywgTG9jYWxlQ29uZmlnIH0gZnJvbSAnLi9kYXRlcmFuZ2VwaWNrZXIuY29uZmlnJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvY2FsZVNlcnZpY2Uge1xuICBjb25zdHJ1Y3RvcihASW5qZWN0KExPQ0FMRV9DT05GSUcpIHByaXZhdGUgX2NvbmZpZzogTG9jYWxlQ29uZmlnKSB7fVxuXG4gIGdldCBjb25maWcoKSB7XG4gICAgaWYgKCF0aGlzLl9jb25maWcpIHtcbiAgICAgIHJldHVybiBEZWZhdWx0TG9jYWxlQ29uZmlnO1xuICAgIH1cblxuICAgIHJldHVybiB7Li4uIERlZmF1bHRMb2NhbGVDb25maWcsIC4uLnRoaXMuX2NvbmZpZ307XG4gIH1cbn1cbiJdfQ==