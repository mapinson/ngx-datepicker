import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DaterangepickerComponent } from './daterangepicker.component';
import { DaterangepickerDirective } from './daterangepicker.directive';
import { LOCALE_CONFIG } from './daterangepicker.config';
import { LocaleService } from './locale.service';
import * as ɵngcc0 from '@angular/core';
var NgxDaterangepickerMd = /** @class */ (function () {
    function NgxDaterangepickerMd() {
    }
    NgxDaterangepickerMd_1 = NgxDaterangepickerMd;
    NgxDaterangepickerMd.forRoot = function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: NgxDaterangepickerMd_1,
            providers: [
                { provide: LOCALE_CONFIG, useValue: config },
                { provide: LocaleService, useClass: LocaleService, deps: [LOCALE_CONFIG] }
            ]
        };
    };
    var NgxDaterangepickerMd_1;
NgxDaterangepickerMd.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: NgxDaterangepickerMd });
NgxDaterangepickerMd.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function NgxDaterangepickerMd_Factory(t) { return new (t || NgxDaterangepickerMd)(); }, providers: [], imports: [[
            CommonModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(NgxDaterangepickerMd, { declarations: function () { return [DaterangepickerComponent,
        DaterangepickerDirective]; }, imports: function () { return [CommonModule,
        FormsModule,
        ReactiveFormsModule]; }, exports: function () { return [DaterangepickerComponent,
        DaterangepickerDirective]; } }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵngcc0.ɵsetClassMetadata(NgxDaterangepickerMd, [{
        type: NgModule,
        args: [{
                declarations: [
                    DaterangepickerComponent,
                    DaterangepickerDirective
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                providers: [],
                exports: [
                    DaterangepickerComponent,
                    DaterangepickerDirective
                ],
                entryComponents: [
                    DaterangepickerComponent
                ]
            }]
    }], function () { return []; }, null); })();
    return NgxDaterangepickerMd;
}());
export { NgxDaterangepickerMd };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXJhbmdlcGlja2VyLm1vZHVsZS5qcyIsInNvdXJjZXMiOlsibmd4LWRhdGVyYW5nZXBpY2tlci1tYXRlcmlhbC9kYXRlcmFuZ2VwaWNrZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUF3QixRQUFRLEVBQXNCLE1BQU0sZUFBZSxDQUFDO0FBQ25GLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUVqRSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUN2RSxPQUFPLEVBQWdCLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7QUFxQmpEO0lBQ0U7SUFDQSxDQUFDOzZCQUZVLG9CQUFvQjtJQUd4Qiw0QkFBTyxHQUFkLFVBQWUsTUFBeUI7UUFBekIsdUJBQUEsRUFBQSxXQUF5QjtRQUN0QyxPQUFPO1lBQ0wsUUFBUSxFQUFFLHNCQUFvQjtZQUM5QixTQUFTLEVBQUU7Z0JBQ1QsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUM7Z0JBQzNDLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLGFBQWEsQ0FBQyxFQUFDO2FBQzFFO1NBQ0YsQ0FBQztJQUNKLENBQUM7K0JBRTJCO0lBYmpCLG9CQUFvQixpREFuQmhDLFFBQVEsQ0FBQztXQUNSLFlBQVksRUFBRSxrQkFDWix3QkFBd0Isa0JBQ3hCLHdCQUF3QixjQUN6QixjQUNELE9BQU8sRUFBRSxrQkFDUCxZQUFZO2dCQUNaO0NBQVcsa0JBQ1g7YUFBbUIsY0FDcEI7U0FDRDtHQUFTLEVBQUUsRUFBRSxjQUNiLE9BQU8sRUFBRSxrQkFDUCx3QkFBd0Isa0JBQ3hCLHdCQUF3QixjQUN6QixjQUNELGVBQWUsRUFBRSxrQkFDZjt1QkFBd0IsY0FDekIsVUFDRixDQUFDLFFBQ1csb0JBQW9CLENBWWhDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2dEQUNEO0lBREEsMkJBQUM7Q0FBQSxBQVpELElBWUM7U0FaWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgIE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBPcHRpb25hbCwgU2tpcFNlbGYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IERhdGVyYW5nZXBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4vZGF0ZXJhbmdlcGlja2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlcmFuZ2VwaWNrZXJEaXJlY3RpdmUgfSBmcm9tICcuL2RhdGVyYW5nZXBpY2tlci5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgTG9jYWxlQ29uZmlnLCBMT0NBTEVfQ09ORklHIH0gZnJvbSAnLi9kYXRlcmFuZ2VwaWNrZXIuY29uZmlnJztcbmltcG9ydCB7IExvY2FsZVNlcnZpY2UgfSBmcm9tICcuL2xvY2FsZS5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGF0ZXJhbmdlcGlja2VyQ29tcG9uZW50LFxuICAgIERhdGVyYW5nZXBpY2tlckRpcmVjdGl2ZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXSxcbiAgZXhwb3J0czogW1xuICAgIERhdGVyYW5nZXBpY2tlckNvbXBvbmVudCxcbiAgICBEYXRlcmFuZ2VwaWNrZXJEaXJlY3RpdmVcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBbXG4gICAgRGF0ZXJhbmdlcGlja2VyQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTmd4RGF0ZXJhbmdlcGlja2VyTWQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuICBzdGF0aWMgZm9yUm9vdChjb25maWc6IExvY2FsZUNvbmZpZyA9IHt9KTogTW9kdWxlV2l0aFByb3ZpZGVyczxOZ3hEYXRlcmFuZ2VwaWNrZXJNZD4ge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogTmd4RGF0ZXJhbmdlcGlja2VyTWQsXG4gICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgeyBwcm92aWRlOiBMT0NBTEVfQ09ORklHLCB1c2VWYWx1ZTogY29uZmlnfSxcbiAgICAgICAgeyBwcm92aWRlOiBMb2NhbGVTZXJ2aWNlLCB1c2VDbGFzczogTG9jYWxlU2VydmljZSwgZGVwczogW0xPQ0FMRV9DT05GSUddfVxuICAgICAgXVxuICAgIH07XG4gIH1cbn1cbiJdfQ==