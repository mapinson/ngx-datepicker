import { ViewContainerRef, ComponentFactoryResolver, ElementRef, ChangeDetectorRef, OnInit, OnChanges, SimpleChanges, DoCheck, KeyValueDiffers, EventEmitter, Renderer2 } from '@angular/core';
import { DaterangepickerComponent } from './daterangepicker.component';
import * as _moment from 'moment';
import { LocaleConfig } from './daterangepicker.config';
import { LocaleService } from './locale.service';
import * as ɵngcc0 from '@angular/core';
export declare class DaterangepickerDirective implements OnInit, OnChanges, DoCheck {
    viewContainerRef: ViewContainerRef;
    _changeDetectorRef: ChangeDetectorRef;
    private _componentFactoryResolver;
    private _el;
    private _renderer;
    private differs;
    private _localeService;
    private elementRef;
    picker: DaterangepickerComponent;
    private _onChange;
    private _onTouched;
    private _validatorChange;
    private _disabled;
    private _value;
    private localeDiffer;
    minDate: _moment.Moment;
    maxDate: _moment.Moment;
    autoApply: boolean;
    alwaysShowCalendars: boolean;
    showCustomRangeLabel: boolean;
    linkedCalendars: boolean;
    dateLimit: number;
    singleDatePicker: boolean;
    showWeekNumbers: boolean;
    showISOWeekNumbers: boolean;
    showDropdowns: boolean;
    isInvalidDate: Function;
    isCustomDate: Function;
    isTooltipDate: Function;
    showClearButton: boolean;
    customRangeDirection: boolean;
    ranges: any;
    opens: string;
    drops: string;
    firstMonthDayClass: string;
    lastMonthDayClass: string;
    emptyWeekRowClass: string;
    emptyWeekColumnClass: string;
    firstDayOfNextMonthClass: string;
    lastDayOfPreviousMonthClass: string;
    keepCalendarOpeningWithRange: boolean;
    showRangeLabelOnInput: boolean;
    showCancel: boolean;
    lockStartDate: boolean;
    timePicker: Boolean;
    timePicker24Hour: Boolean;
    timePickerIncrement: number;
    timePickerSeconds: Boolean;
    closeOnAutoApply: boolean;
    _locale: LocaleConfig;
    set locale(value: any);
    get locale(): any;
    private _endKey;
    private _startKey;
    set startKey(value: any);
    set endKey(value: any);
    notForChangesProperty: Array<string>;
    get value(): any;
    set value(val: any);
    onChange: EventEmitter<Object>;
    rangeClicked: EventEmitter<Object>;
    datesUpdated: EventEmitter<Object>;
    startDateChanged: EventEmitter<Object>;
    endDateChanged: EventEmitter<Object>;
    get disabled(): boolean;
    constructor(viewContainerRef: ViewContainerRef, _changeDetectorRef: ChangeDetectorRef, _componentFactoryResolver: ComponentFactoryResolver, _el: ElementRef, _renderer: Renderer2, differs: KeyValueDiffers, _localeService: LocaleService, elementRef: ElementRef);
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngDoCheck(): void;
    onBlur(): void;
    open(event?: any): void;
    hide(e?: any): void;
    toggle(e?: any): void;
    clear(): void;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    setDisabledState(state: boolean): void;
    private setValue;
    /**
     * Set position of the calendar
     */
    setPosition(): void;
    inputChanged(e: any): void;
    /**
     * For click outside of the calendar's container
     * @param event event object
     */
    outsideClick(event: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DaterangepickerDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<DaterangepickerDirective, "input[ngxDaterangepickerMd]", never, { "dateLimit": "dateLimit"; "showCancel": "showCancel"; "lockStartDate": "lockStartDate"; "timePicker": "timePicker"; "timePicker24Hour": "timePicker24Hour"; "timePickerIncrement": "timePickerIncrement"; "timePickerSeconds": "timePickerSeconds"; "closeOnAutoApply": "closeOnAutoApply"; "_endKey": "_endKey"; "drops": "drops"; "opens": "opens"; "locale": "locale"; "startKey": "startKey"; "endKey": "endKey"; "minDate": "minDate"; "maxDate": "maxDate"; "autoApply": "autoApply"; "alwaysShowCalendars": "alwaysShowCalendars"; "showCustomRangeLabel": "showCustomRangeLabel"; "linkedCalendars": "linkedCalendars"; "singleDatePicker": "singleDatePicker"; "showWeekNumbers": "showWeekNumbers"; "showISOWeekNumbers": "showISOWeekNumbers"; "showDropdowns": "showDropdowns"; "isInvalidDate": "isInvalidDate"; "isCustomDate": "isCustomDate"; "isTooltipDate": "isTooltipDate"; "showClearButton": "showClearButton"; "customRangeDirection": "customRangeDirection"; "ranges": "ranges"; "lastMonthDayClass": "lastMonthDayClass"; "emptyWeekRowClass": "emptyWeekRowClass"; "emptyWeekColumnClass": "emptyWeekColumnClass"; "firstDayOfNextMonthClass": "firstDayOfNextMonthClass"; "lastDayOfPreviousMonthClass": "lastDayOfPreviousMonthClass"; "keepCalendarOpeningWithRange": "keepCalendarOpeningWithRange"; "showRangeLabelOnInput": "showRangeLabelOnInput"; }, { "onChange": "change"; "rangeClicked": "rangeClicked"; "datesUpdated": "datesUpdated"; "startDateChanged": "startDateChanged"; "endDateChanged": "endDateChanged"; }, never>;
}

//# sourceMappingURL=daterangepicker.directive.d.ts.map