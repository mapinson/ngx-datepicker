import { ModuleWithProviders } from '@angular/core';
import { LocaleConfig } from './daterangepicker.config';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './daterangepicker.component';
import * as ɵngcc2 from './daterangepicker.directive';
import * as ɵngcc3 from '@angular/common';
import * as ɵngcc4 from '@angular/forms';
export declare class NgxDaterangepickerMd {
    constructor();
    static forRoot(config?: LocaleConfig): ModuleWithProviders<NgxDaterangepickerMd>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NgxDaterangepickerMd, [typeof ɵngcc1.DaterangepickerComponent, typeof ɵngcc2.DaterangepickerDirective], [typeof ɵngcc3.CommonModule, typeof ɵngcc4.FormsModule, typeof ɵngcc4.ReactiveFormsModule], [typeof ɵngcc1.DaterangepickerComponent, typeof ɵngcc2.DaterangepickerDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NgxDaterangepickerMd>;
}

//# sourceMappingURL=daterangepicker.module.d.ts.map