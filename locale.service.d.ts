import { LocaleConfig } from './daterangepicker.config';
import * as ɵngcc0 from '@angular/core';
export declare class LocaleService {
    private _config;
    constructor(_config: LocaleConfig);
    get config(): LocaleConfig;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<LocaleService, never>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<LocaleService>;
}

//# sourceMappingURL=locale.service.d.ts.map