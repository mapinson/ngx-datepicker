import { __decorate, __param } from "tslib";
import { Injectable, Inject } from '@angular/core';
import { LOCALE_CONFIG, DefaultLocaleConfig } from './daterangepicker.config';
import * as ɵngcc0 from '@angular/core';
let LocaleService = class LocaleService {
    constructor(_config) {
        this._config = _config;
    }
    get config() {
        if (!this._config) {
            return DefaultLocaleConfig;
        }
        return Object.assign(Object.assign({}, DefaultLocaleConfig), this._config);
    }
};
LocaleService.ɵfac = function LocaleService_Factory(t) { return new (t || LocaleService)(ɵngcc0.ɵɵinject(LOCALE_CONFIG)); };
LocaleService.ɵprov = ɵngcc0.ɵɵdefineInjectable({ token: LocaleService, factory: function (t) { return LocaleService.ɵfac(t); } });
LocaleService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
];
LocaleService = __decorate([ __param(0, Inject(LOCALE_CONFIG))
], LocaleService);
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵngcc0.ɵsetClassMetadata(LocaleService, [{
        type: Injectable
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [LOCALE_CONFIG]
            }] }]; }, null); })();
export { LocaleService };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlLnNlcnZpY2UuanMiLCJzb3VyY2VzIjpbIm5neC1kYXRlcmFuZ2VwaWNrZXItbWF0ZXJpYWwvbG9jYWxlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxhQUFhLEVBQUUsbUJBQW1CLEVBQWdCLE1BQU0sMEJBQTBCLENBQUM7O0FBRzVGLElBQWEsYUFBYSxHQUExQixNQUFhLGFBQWE7SUFDeEIsWUFBMkMsT0FBcUI7UUFBckIsWUFBTyxHQUFQLE9BQU8sQ0FBYztJQUFHLENBQUM7SUFFcEUsSUFBSSxNQUFNO1FBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDakIsT0FBTyxtQkFBbUIsQ0FBQztTQUM1QjtRQUVELHVDQUFZLG1CQUFtQixHQUFLLElBQUksQ0FBQyxPQUFPLEVBQUU7SUFDcEQsQ0FBQztDQUNGOzttSUFBQTs7NENBVGMsTUFBTSxTQUFDLGFBQWE7O0FBRHRCLGFBQWEsZUFDZCxLQUZYLFVBQVUsRUFBRSxqQkFDVCxDQUNXLFdBQUEsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0dBRHZCLGFBQWEsQ0FVekI7Ozs7OztrQ0FDRDtTQVhhLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExPQ0FMRV9DT05GSUcsIERlZmF1bHRMb2NhbGVDb25maWcsIExvY2FsZUNvbmZpZyB9IGZyb20gJy4vZGF0ZXJhbmdlcGlja2VyLmNvbmZpZyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2NhbGVTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoQEluamVjdChMT0NBTEVfQ09ORklHKSBwcml2YXRlIF9jb25maWc6IExvY2FsZUNvbmZpZykge31cblxuICBnZXQgY29uZmlnKCkge1xuICAgIGlmICghdGhpcy5fY29uZmlnKSB7XG4gICAgICByZXR1cm4gRGVmYXVsdExvY2FsZUNvbmZpZztcbiAgICB9XG5cbiAgICByZXR1cm4gey4uLiBEZWZhdWx0TG9jYWxlQ29uZmlnLCAuLi50aGlzLl9jb25maWd9O1xuICB9XG59XG4iXX0=